const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp()
const db = admin.database()
/**

Game: {
  createdAt
  players []
  status
  whoseTurn
  previousMove {
    player
    what you heard / saw
  }
}

 */
// Creates a new game
exports.createGame = functions.https.onCall((data, context) => {
  const newGame = db.ref('games').push()

  const gameData = {
    createdAt: context.timestamp,
    ownerId: context.auth.currentUser.uid,
    players: {
      1: context.auth.currentUser.uid
    },
    status: 'created'
  }

  return newGame.set(gameData).then((success) => {
    return { gameId: newGame.getKey() }
  }).catch((failed) => {
    return { err: 'failed to create new game' }
  })
})

// exports.getGames = functions.https.onCall((data, context) => {

// })

// exports.joinGame = functions.https.onCall((data, context) => {

// })

// exports.startGame = functions.https.onCall((data, context) => {
//   // are you the owner?

// })

// exports.takeTurn = functions.https.onCall((data, context) => {

// })
