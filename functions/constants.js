export const SPACE_TYPES = {
  SPACE: 'SPACE',
  CITADEL: 'CITADEL',
  TOMB: 'TOMB',
  RUIN: 'RUIN',
  BAZAAR: 'BAZAAR',
  SANCTUARY: 'SANTUARY',
  NEXT_FRONTIER: 'NEXT_FRONTIER',
  PREV_FRONTIER: 'PREV_FRONTIER',
  DARK_TOWER: 'DARK_TOWER'
}

export const BOARD = {
  1: { type: SPACE_TYPES.SPACE, moveTo: [0, 2, 6, 7] },
  2: { type: SPACE_TYPES.SPACE, moveTo: [1, 3, 7, 8] },
  3: { type: SPACE_TYPES.SPACE, moveTo: [2, 4, 8] },
  4: { type: SPACE_TYPES.SPACE, moveTo: [3, 5, 8, 9, 12, 13] },
  5: { type: SPACE_TYPES.SPACE, moveTo: [4, 9, 32] },
  6: { type: SPACE_TYPES.SPACE, moveTo: [1, 7, 10, 11] },
  7: { type: SPACE_TYPES.SPACE, moveTo: [1, 2, 6, 8, 10, 11] },
  8: { type: SPACE_TYPES.TOMB, moveTo: [2, 3, 4, 7, 11, 12] },
  9: { type: SPACE_TYPES.SPACE, moveTo: [4, 5, 13, 14, 32] },
  10: { type: SPACE_TYPES.SPACE, moveTo: [6, 11, 12, 15] },
  11: { type: SPACE_TYPES.SPACE, moveTo: [6, 7, 8, 10, 12] },
  12: { type: SPACE_TYPES.SPACE, moveTo: [4, 8, 10, 11, 13, 15, 16, 17, 18] },
  13: { type: SPACE_TYPES.BAZAAR, moveTo: [4, 9, 12, 14, 18, 23, 24] },
  14: { type: SPACE_TYPES.SPACE, moveTo: [9, 13, 24, 32] },
  15: { type: SPACE_TYPES.CITADEL, moveTo: [10, 12, 16, 19, 20] },
  16: { type: SPACE_TYPES.SPACE, moveTo: [12, 15, 17, 20] },
  17: { type: SPACE_TYPES.SPACE, moveTo: [12, 16, 18, 20, 21] },
  18: { type: SPACE_TYPES.SPACE, moveTo: [12, 13, 17, 21, 22, 23] },
  19: { type: SPACE_TYPES.SPACE, moveTo: [15, 20, 25, 27] },
  20: { type: SPACE_TYPES.SPACE, moveTo: [15, 16, 17, 19, 21, 25] },
  21: { type: SPACE_TYPES.SPACE, moveTo: [17, 18, 20, 22, 25, 26] },
  22: { type: SPACE_TYPES.SPACE, moveTo: [18, 21, 23, 26, 30, 31] },
  23: { type: SPACE_TYPES.SPACE, moveTo: [13, 18, 22, 24, 31] },
  24: { type: SPACE_TYPES.SPACE, moveTo: [13, 14, 23, 31, 32] },
  25: { type: SPACE_TYPES.SPACE, moveTo: [19, 20, 21, 26, 27, 28] },
  26: { type: SPACE_TYPES.RUIN, moveTo: [21, 22, 25, 28, 29, 30] },
  27: { type: SPACE_TYPES.SANCTUARY, moveTo: [19, 25, 28] },
  28: { type: SPACE_TYPES.SPACE, moveTo: [25, 26, 27, 29, 33] },
  29: { type: SPACE_TYPES.SPACE, moveTo: [26, 28, 30, 33] },
  30: { type: SPACE_TYPES.SPACE, moveTo: [22, 26, 29, 31, 33] },
  31: { type: SPACE_TYPES.SPACE, moveTo: [22, 23, 24, 33] },
  32: { type: SPACE_TYPES.DARK_TOWER, moveTo: [5, 9, 14, 24] },
  33: { type: SPACE_TYPES.NEXT_FRONTIER, moveTo: [] } // move to next territory
}

const data = {
  playerPosition: 2,
  requestedMove: 9,
  gameBoard: {
    1: { type: SPACE_TYPES.SPACE, moveTo: [0, 2, 6, 7] },
    2: { type: SPACE_TYPES.SPACE, moveTo: [1, 3, 7, 8] },
    3: { type: SPACE_TYPES.SPACE, moveTo: [2, 4, 8] },
    4: { type: SPACE_TYPES.SPACE, moveTo: [3, 5, 8, 9, 12, 13] },
    5: { type: SPACE_TYPES.SPACE, moveTo: [4, 9, 32] },
    6: { type: SPACE_TYPES.SPACE, moveTo: [1, 7, 10, 11] },
    7: { type: SPACE_TYPES.SPACE, moveTo: [1, 2, 6, 8, 10, 11] }
  }
}
