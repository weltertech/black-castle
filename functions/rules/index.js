import { and, or } from 'regent'
/*

player: {
    id
    territory:
    position: { type: space_type, moveTo: [0,1,2] }
    inventory: {
        gold: int
        warriors: int
        food: int

        sword:  bool
        pegasus: bool
        beast: bool
        healer: bool
        scout: bool
    }
    current: (cursed, dragon, battle, bazaar, tower)
}

game: {
    turn: (string user id)
    createdAt:
    ownerId:
    players: {
      1: context.auth.currentUser.uid
    },
    status: (created, ongoing, done, abandoned),
    board:
}

board:

move: {
    territory:
    position:
}
*/

export const isMyTurn = { left: '@data.player.id', fn: 'equals', right: '@data.game.turn' }

const isValidPositionMove = { left: '@data.player.position.moveTo', fn: 'includes', right: '@data.move.position' }
const isSameTerritory = { left: '@data.player.territory', fn: 'includes', right: '@data.move.territory' }

export const isValidMove = and(isValidPositionMove, isSameTerritory)
