Posts - write when authenticated - update only some fields if owner
  - title
  - candidates
  - created at
  - owner
  - ispublic
  - isactive

UserActivity (this info only updated by server fn)
  - userid  (ready only by user)
    - active post count (only needed to prevent users from creating tons of stuff at once)

Votes
  - post id (write if votes.post.user doesnt exist)
    - user id
    - ranks


sample data

{
  posts: {
    "post123": {
      "title": "my title",
      "candidates": {
        "123412341351": { "id": "zzz1111", "name": "option1" },
        "134135143512": { "id": "zzz2222", "name": "option2" },
      },
      "isPublic": true, // allow owner to edit
      "isActive": true, // allow owner to edit
      "expiresAt": "?",
      "createdAt": "12324352462346", // timestamp
      "owner": "user123", // user id
    }
  },
  userActivity: {
    "user123": {
      "activePostCount": 1,
    }
  },
  votes: {
    "post123": { // post id
      "user123": { // user id
        "votedAt": "12324352462346", // timestamp
        "ranks": [
          "0": {
            idx: 0,
            candidateId: "134135143512",
          }
        ]
      }
    }
  }

