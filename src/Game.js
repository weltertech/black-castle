import React, { Component } from 'react'
import firebase from 'firebase'
import PropTypes from 'prop-types'
import { AuthConsumer } from './auth/AuthContext'

class Game extends Component {
  constructor (props) {
    super(props)
    this.state = {
      game: undefined
    }
  }

  componentDidMount () {
    this.dbGameRef = firebase.database().ref(`posts/${this.props.match.params.postulationId}`)
    this.dbGameRef.on('value', (gameSnapshot) => {
      if (gameSnapshot) {
        this.setState({ game: { ...gameSnapshot.val(), id: this.props.match.params.gameId } })
      }
    })
  }

  componentWillUnmount () {
    this.dbGameRef.off()
  }

  render () {
    return (
      <AuthConsumer>
        {({ user }) => (
          <div className='App'>
            {this.state.game ? this.state.game.createdAt : null}
          </div>
        )}
      </AuthConsumer>
    )
  }
}

Game.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object
}

export default Game
