import React from 'react'
import PropTypes from 'prop-types'
import ReactSound from 'react-sound'
import Button from '@material-ui/core/Button'

import _map from 'lodash.map'

export const SOUNDS = {
  BATTLE: 'battle.wav',
  BAZAAR_CLOSED: 'bazaar-closed.wav',
  BAZAAR: 'bazaar.wav',
  BEEP: 'beep.wav',
  CLEAR: 'clear.wav',
  DARK_TOWER: 'darktower.wav',
  DRAGON_KILL: 'dragon-kill.wav',
  DRAGON: 'dragon.wav',
  END_TURN: 'end-turn.wav',
  ENEMY_HIT: 'enemy-hit.wav',
  FRONTIER: 'frontier.wav',
  INTRO: 'intro.wav',
  LOST: 'lost.wav',
  PEGASUS: 'pegasus.wav',
  PLAGUE: 'plague.wav',
  PLAYER_HIT: 'player-hit.wav',
  ROTATE: 'rotate.wav',
  SANCTUARY: 'sanctuary.wav',
  STARVING: 'starving.wav',
  TOMB_BATTLE: 'tomb-battle.wav',
  TOMB_NOTHING: 'tomb-nothing.wav',
  TOMB: 'tomb.wav',
  WRONG: 'wrong.wav'
}

class Sounds extends React.Component {
  constructor () {
    super()
    this.playSound = this.playSound.bind(this)

    this.state = {
      sound: SOUNDS.BEEP
    }
  }

  playSound (clickEvent) {
    console.log('======================> click event', clickEvent)
    this.setState({
      sound: clickEvent.currentTarget.value
    })
  }

  render () {
    return (
      <div>
        <ReactSound playStatus={ReactSound.status.PLAYING} url={`/audio/${this.state.sound}`} />
        {_map(SOUNDS, (sound) => {
          return <Button onClick={this.playSound} value={sound}>{sound}</Button>
        })}
      </div>
    )
  }
}

Sounds.propTypes = {
  children: PropTypes.element
}

export default Sounds
