import React from 'react'
import ReactDOM from 'react-dom'
import firebase from 'firebase'
import Board from './Board'
import Sounds from './Sounds'
import Game from './Game'
import Header from './components/Header'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
import { AuthProvider, AuthConsumer } from './auth/AuthContext'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAcetOTIfSIoXPmHNd7jM-UntBgAodcGyM',
  authDomain: 'black-castle-3b124.firebaseapp.com',
  databaseURL: 'https://black-castle-3b124.firebaseio.com',
  projectId: 'black-castle-3b124',
  storageBucket: 'black-castle-3b124.appspot.com',
  messagingSenderId: '977923715334'
}

try {
  firebase.initializeApp(config)
} catch (error) {
  // already initialized
}

ReactDOM.render((
  <BrowserRouter>
    <AuthProvider>
      <Header />
      <AuthConsumer>
        {({ user }) => (
          <Switch>
            { !user && <Route exact path='/board' component={Board} /> }
            { !user && <Route exact path='/' component={Sounds} /> }
            { user && <Route exact path='/game/{gameId}' component={Game} /> }
            {/* { user && <Route exact path='/games' component={Games} /> } */}
          </Switch>
        )}
      </AuthConsumer>
    </AuthProvider>
  </BrowserRouter>
), document.getElementById('root'))

registerServiceWorker()
