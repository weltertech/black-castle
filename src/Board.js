import React, { Component } from 'react'
import PropTypes from 'prop-types'

const getScaleData = (screenWidth) => {
  const halfScale = screenWidth >= 600

  return {
    topOffset: halfScale ? `-${screenWidth / 2}px` : `-${screenWidth}px`,
    leftOffset: halfScale ? 0 : `-${screenWidth}px`,
    imgWidth: halfScale ? '100%' : screenWidth * 2,
    halfScale
  }
}

class Game extends Component {
  constructor (props) {
    super(props)
    this.state = {
      rotation: 0,
      mouseDown: false,
      x: 0,
      y: 0,
      ...getScaleData(window.innerWidth)
    }
    this.onMouseDown = this.onMouseDown.bind(this)
    this.onMouseUp = this.onMouseUp.bind(this)
    this.onMouseMove = this.onMouseMove.bind(this)
    this.updateOffset = this.updateOffset.bind(this)
  }

  updateOffset () {
    console.log(getScaleData(window.innerWidth))
    this.setState({
      ...getScaleData(window.innerWidth)
    })
  }

  componentDidMount () {
    window.addEventListener('resize', this.updateOffset)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.updateOffset)
  }

  onMouseDown (e, isTouch = false) {
    e.preventDefault()
    e.stopPropagation()
    console.log('onMouseDown', e.screenX, e.screenY)
    this.setState({
      mouseDown: true,
      x: isTouch ? e.touches[0].screenX : e.screenX,
      y: isTouch ? e.touches[0].screenY : e.screenY
    })
  }

  onMouseUp (e, isTouch = false) {
    e.preventDefault()
    e.stopPropagation()
    console.log('onMouseUp')
    this.setState({
      mouseDown: false
    })
  }

  onMouseMove (e, isTouch = false) {
    e.preventDefault()
    e.stopPropagation()
    const xPos = isTouch ? e.touches[0].screenX : e.screenX
    if (this.state.mouseDown) {
      const rotation = (this.state.x - xPos / 2)
      this.setState({
        rotation
      })
    }
  }

  render () {
    const rotateValue = `rotate(${this.state.rotation}deg)`

    const rotatedStyle = {
      width: this.state.imgWidth,
      '-ms-transform': rotateValue, /* IE 9 */
      '-webkit-transform': rotateValue, /* Safari */
      transform: rotateValue,
      top: this.state.topOffset,
      left: this.state.leftOffset
    }

    const imgStyle = {
      width: this.state.imgWidth
    }

    const detectDragStyle = {
      width: this.state.imgWidth,
      marginTop: this.state.topOffset,
      marginLeft: this.state.leftOffset
    }

    return (
      <div
        style={detectDragStyle}
        onMouseDown={this.onMouseDown}
        onMouseUp={this.onMouseUp}
        onMouseMove={this.onMouseMove}
        onMouseOut={this.onMouseUp}
        onTouchStart={(e) => this.onMouseDown(e, true)}
        onTouchMove={(e) => this.onMouseMove(e, true)}
        onTouchEnd={(e) => this.onMouseUp(e, true)}
      >
        <div style={rotatedStyle}><img alt='' style={imgStyle} draggable={false} src='/images/board.png' /></div>
      </div>
    )
  }
}

Game.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object
}

export default Game
