import React from 'react'
import firebase from 'firebase'
import PropTypes from 'prop-types'

const AuthContext = React.createContext()

class AuthProvider extends React.Component {
  constructor () {
    super()
    this.handleFacebookSuccess = this.handleFacebookSuccess.bind(this)
    this.handleFacebookError = this.handleFacebookError.bind(this)
    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
    this.state = { user: undefined }
  }

  componentDidMount () {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })
  }

  handleFacebookSuccess (result) {
    // The signed-in user info.
    var user = result.user
    this.setState({ user })
  }

  handleFacebookError (error) {
    console.log(JSON.stringify(error, null, 2))
  }

  login () {
    const provider = new firebase.auth.FacebookAuthProvider()
    firebase.auth().signInWithPopup(provider)
      .then(result => this.handleFacebookSuccess(result))
      .catch(error => this.handleFacebookError(error))
  }

  logout () {
    firebase.auth().signOut()
      .then(() => {
        this.setState({
          user: null
        })
      })
  }

  render () {
    return (
      <AuthContext.Provider
        value={{ user: this.state.user, login: this.login, logout: this.logout }}
      >
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}
const AuthConsumer = AuthContext.Consumer

AuthProvider.propTypes = {
  children: PropTypes.element
}

export { AuthProvider, AuthConsumer }
