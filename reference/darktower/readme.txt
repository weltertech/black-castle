Setup
-----

To run the Dark Tower game you need the Java Runtime Environment J2SE 1.4.0 (JRE)
or the Java Software Development Kit J2SE 1.4.0 (SDK).
You can download it from 

	http://java.sun.com/downloads.html

After installing the Java Runtime Environment you can start the game by 
simple double clicking on the "darktower.jar" file.
If this does not work you can start the game manually with the following
command entered at the DOS-Prompt:

	javaw.exe -jar darktower.jar

First you must change to the directory where you have copied the
"darktower.jar" file. This can be done with the "CD <directory>" command.
Alternative you can use the "darktower.cmd" batchfile to start the game.


How to play
-----------

In The "File" menu you have the following options:
- New
  Starts a new Dark Tower game.
- Board
  You have the choice to play without the board. If you want this simple
  close the "Board" windows by clicking on the [x] in the top right corner.
  If you want to open the "Board" window again select this menu option.
- Options
  This opens the "Options" window. Here you can select the number of players
  and determine which of them should controlled by the computer.
- Inventory
  This opens the "Inventory" window, which displays the items collected by
  the four players.

If you move with the mouse over the board the fields that you can enter are
highlighted. After clicking in the field of your choice the pawn moves to
this field and the action correspondending with this field will automatically
take place. To finish your move press the "No/End" button.


Credit
------

The graphics used for the boarddesign are (c)Copyright by David E. Gervais,
and used with his express permission.
